name := "intern"

version := "0.1"

scalaVersion in ThisBuild := "2.13.4"

inThisBuild {
  List(
    scalaVersion := "2.13.4"
  )
}

libraryDependencies ++= Seq("org.typelevel" %% "cats-effect" % "2.5.1"  )
scalacOptions ++= Seq("-feature", "-language:implicitConversions", "-Xfatal-warnings", "-Ymacro-annotations")

